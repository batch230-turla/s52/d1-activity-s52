//import logo from './logo.svg';
import './App.css';
import AppNavbar from './components/AppNavbar'

import Register from './pages/Register'
//import Banner from './components/Banner'
//import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import Home from './pages/Home';
import {Container} from 'react-bootstrap';


import {Fragment } from 'react'

function App() {
  return (
    <Fragment>
      <AppNavbar />
      <Container>
          <Home />
          <Courses/>
          <Register />
      </Container>
    
    </Fragment>
  );
}

export default App;
